// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBWK4eWeo9ccsa2TcLiNFlyM0dR4mITtdE",
    authDomain: "fir-tutorial1-bec3d.firebaseapp.com",
    databaseURL: "https://fir-tutorial1-bec3d.firebaseio.com",
    projectId: "fir-tutorial1-bec3d",
    storageBucket: "fir-tutorial1-bec3d.appspot.com",
    messagingSenderId: "865346397435",
    appId: "1:865346397435:web:36732dee64d0250968fc5e"
  }
};