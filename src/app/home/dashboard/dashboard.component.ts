import { Component, OnInit } from '@angular/core';
import { firestoreService } from 'src/app/source/services/firebase.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  coalData: any;
  obData: any;
  exc_list :any;
  shift1OB: number = 0;
  shift2OB: number = 0;
  shift3OB: number = 0;
  total_tripOB: number = 0;
  shift1: number = 0;
  shift2: number = 0;
  shift3: number = 0;
  total_trip: number = 0;

  dbData: Object = { shift1: 0, shift2: 0, shift3: 0, total_trip: 0 };
  

  constructor(private db: firestoreService, private datePipe: DatePipe) {
  }


  ngOnInit() {
    console.log('page loaded')
    this.db.subject.subscribe(
      (data => {
        this.dbData = data;
        //console.log(this.dbData);
        this.shift1 = data["shift1"];
        this.shift2 = data["shift2"];
        this.shift3 = data["shift3"];
        this.total_trip = data["total_trip"];
        this.shift1OB = data["shift1OB"];
        this.shift2OB = data["shift2OB"];
        this.shift3OB = data["shift3OB"];
        this.total_tripOB = data["total_tripOB"];
        document.getElementById('shift1_html').innerHTML = String(this.shift1)
        document.getElementById('shift2_html').innerHTML = String(this.shift2)
        document.getElementById('shift3_html').innerHTML = String(this.shift3)
        document.getElementById('total_trip').innerHTML = String(this.total_trip)
        document.getElementById('total_tripOB').innerHTML = String(this.total_tripOB)
        document.getElementById('shift1OB').innerHTML = String(this.shift1OB)
        document.getElementById('shift2OB').innerHTML = String(this.shift2OB)
        document.getElementById('shift3OB').innerHTML = String(this.shift3OB)
      }).bind(this));
    this.db.getData();


  }


}
