import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Subject } from 'rxjs';
import { DatePipe } from '@angular/common';


@Injectable({
    providedIn: 'root'
})

export class firestoreService {

    shift1: number = 0;
    shift2: number = 0;
    shift3: number = 0;
    total_trip: number = 0;
    total_trip_coal_copy: number = 0;
    total_trip_ob_copy: number = 0;
    shift1OB: number = 0;
    shift2OB: number = 0;
    shift3OB: number = 0;
    total_tripOB: number = 0;
    requiredCoalRate: number = 0;
    requiredOBRate: number = 0;
    coalProductionTargetFirebase: number =0;
    obProductionTargetFirebase: number = 0;

    coalMultiplicationFactor: number = 25;
    obMultiplicationFactor: number = 15.5;

    public subject = new Subject();
    
    constructor(private db: AngularFireDatabase, private datePipe: DatePipe ) { }

    object = { shift1: 0, shift2: 0, shift3: 0, total_trip: 0, shift1OB: 0, shift2OB: 0, shift3OB: 0, total_tripOB: 0 };

    getData() {
        this.db.database.ref().child("Durgapur Site- MonteCarlo").on('value', (a, b) => {
            // this.updateValue(a);
        });
    }


}