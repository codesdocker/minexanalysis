import { firestoreService } from './source/services/firebase.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NavbarComponent } from './home/navbar/navbar.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSpinnerModule } from "ngx-spinner";
import { AsideComponent } from './home/aside/aside.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from '../environments/environment';
import { DashboardComponent } from './home/dashboard/dashboard.component';
import { HighchartsChartComponent } from 'highcharts-angular';
import {DatePipe} from '@angular/common';
import { LottieAnimationViewModule } from 'ng-lottie';


@NgModule({
  declarations: [
    AppComponent,
    AsideComponent,
    NavbarComponent,
    HomeComponent,
    DashboardComponent,
    HighchartsChartComponent,

  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxSpinnerModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    TabsModule.forRoot(),
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    LottieAnimationViewModule.forRoot(),
   
  ], 
  providers: [firestoreService,DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
